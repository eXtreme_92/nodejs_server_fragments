var webSocketServer = require('websocket').server;
var http = require('http');
var mysql     =    require('mysql');
var graph = require('fbgraph');


var webSocketsServerPort = '';


var pool      =    mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : '',
    password : '',
    database : '',
    debug    :  false
});


/**
 * HTTP server
 */
var server = http.createServer(function(request, response) { });

server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Nasłuchiwanie na porcie " + webSocketsServerPort);
});



/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    httpServer: server
});





//global variables



var users = {}; 


wsServer.on('request', function(request) {
    
		console.log('connection', Object.keys(users).length);
	
	    if(connection != null) {
			connection.close(); 
			connection = null;
			return;
		}
		

		//data from token

		var index = null;
		var friends = null;
		var name = null;
		var loggedIn = false;
		var connection = connection = request.accept(null, request.origin); 
		var last_id = null; 
		
		

function handle_database(conn, query, opt) {
    
 pool.getConnection(function(err,connection){
        if (err) {
          console.log("Error in connecting to db!");
          return;
        }   

        console.log('connected as id ' + connection.threadId);
        
        connection.query(query,function(err,rows){
            connection.release();
            if(!err) {
				if(typeof opt != 'undefined') {
					for(var opt_index = 0; opt_index < opt.length; opt_index++ ) {

					
						 if(opt[opt_index] == 1) {
							
							try {
								users[index].meetings = rows;
							} catch(ex) {
								console.log("exc: " + ex.toString());
							}
							
						 } 

						 /*
						 
						 
						 
						 
						 
						 ***/
						 
						 if(opt[opt_index] == 4) {
							try {
								for(var el in rows) {
									delete el.userID;
									delete el.id;
								}
								users[index].notifications = rows;
							} catch(ex) {
								console.log("exc: " + ex.toString());
							}
						 }
						 
						
						 
					}
				}
            }           
        });

        connection.on('error', function(err) {      
              console.log("Error in connecting to db!");
              return;     
        });
  });
}		



/* Facebook Token Authentication */	
		
	function check_token(data) {
	
		graph.get("/"+data[1]+"/permissions?access_token="+data[2], function(err, res) {
		console.log(res); 
		
		if(!err && !res.error) {
			index = data[1];
			friends = JSON.parse(data[3]);
            name = data[4]; 
			
			console.log(friends);
			console.log(name);
			console.log(index);
			      		
                 
			users[index] = {connection: connection};
			loggedIn = true;
			
			
			users[index].meetings = [];
			users[index].notifications = [];
/*



***/

	
			
			var query = "select M.* from meetings M join meeting_user MU on M.meetingID = MU.meetingID where MU.userID='"+index+"'";
			
			handle_database(connection,query,[1]); //meetings
			

			
			query = "select N.* from notifications N where N.userID='"+index+"'";
			
			handle_database(connection,query,[4]); //notifications
			
			
/*
		
		
		
***/
			

			
		} 
		
	});
	
}
		
		
		/*  sending msg for specific user  */
		function sendMSGforUser(msg, id) {
			console.log('ok');
			if(loggedIn) {
				console.log('ok1');
					if(users[id] != null) {
						console.log('ok3');
						users[id].connection.sendUTF(JSON.stringify(msg));    
					}
			}
		
		}

		/* sending msg for myself */
		function sendMSGforMe(msg) {
			if(loggedIn) {
					connection.sendUTF(JSON.stringify(msg));
					console.log(msg);
			}
		}
		
		
	/* handling messages */	
    connection.on('message', function(message) {
        if (message.type === 'utf8') { 
            var data = message.utf8Data.split('$');
			      switch(data[0]) {
						    
								/*
								
								
								
								
								
								
								***/
								
								
								case 'delete_meeting': { 
								
								    delete_meeting(data);		
									break;
								}
								
								case 'leave_meeting': { 
								
								    leave_meeting(data);		
									break;
								}
						
						
								/*
								
								
								
								
								
								
								
								
								***/
								
											
				  }
		}
						
						
						
        
    });
		
		

   /* closing connection */
    connection.on('close', function(connection) {
        
					console.log('close');
					
					if(loggedIn){
						delete users[index];
					}
	
						
    });
	
	
	
	/**
	
	
	
	
	
	
	
	
	
	*/
	
	/* TODO: filtering sql params */
	function delete_meeting(data) {
	
		if(loggedIn) {
			
			var meetingID = data[1];
			
		
			var query = "delete from meetings where meetingID='"+meetingID+"'";
			
			handle_database(connection,query);
			
			
		
					
			var invited_friends = users[index].meeting_user.map(function(el){if(el.meetingID == meetingID) return el;});
			invited_friends = invited_friends.filter(function(n){ return n != undefined }); 
			
			
			console.log(invited_friends.toString());
			
			for(var key in invited_friends) {
				
					var msg = [
								{ 
									header: "notification", 
									data: { 
											msg: "Użytkownik "+name+" usunął jedno ze spotkań." 
									} 
								},
								{
									header: "delete_meeting", 
									data: { 
											meetingID: meetingID
									} 
										
								}
									
									
									
							  ];
							  
					sendMSGforUser(msg, invited_friends[key].userID);
					
					
					
					//notifications
			
					var query = "insert into notifications set userID='"+invited_friends[key].userID+"', msg='"+msg[0].data.msg+"'";
					handle_database(connection,query);
					
					
					//
					
					if(users[invited_friends[key].userID] != null) {
					
						users[invited_friends[key].userID].notifications.push(msg[0]);
					
					
				
						for(var key1 in users[invited_friends[key].userID].meetings) {
							if(
								users[invited_friends[key].userID].meetings[key1].meetingID == meetingID
							) {
								users[invited_friends[key].userID].meetings.splice(key1,1);
								break;
							}
						}
				
						
						for(var key1 in users[invited_friends[key].userID].meeting_user) {
							if(
								users[invited_friends[key].userID].meeting_user[key1].meetingID == meetingID
							) {
								users[invited_friends[key].userID].meeting_user.splice(key1,1);
								break;
							}
						}
					
					}
					
			} 
			
			
			
			
			for(var key1 in users[index].meetings) {
						if(
							users[index].meetings[key1].meetingID == meetingID
						) {
							users[index].meetings.splice(key1,1);
							break;
						}
			}
			
			
			
			for(var key1 in users[index].my_meetings) {
						if(
							users[index].my_meetings[key1].meetingID == meetingID
						) {
							users[index].my_meetings.splice(key1,1);
							break;
						}
			}
			
			
		}
		
	}

	/* TODO: filtering sql params */
	function leave_meeting(data) {
	
		if(loggedIn) {
			
			var meetingID = data[1];
			
			var query = "update meeting_user set accept='0' where meetingID='"+meetingID+"' && userID='"+index+"'";
			
			handle_database(connection,query);
			
		
		    for(var key1 in users[index].my_meetings) {
						if(
							users[index].my_meetings[key1].meetingID == meetingID
						) {
							users[index].my_meetings[key1].accept = '0';
							break;
						}
			}
			
			
			
			
			
			var msg = [
								{ 
									header: "notification", 
									data: { 
											msg: "Użytkownik "+name+" zrezygnował z udziału w jednym ze spotkań." 
									} 
								},
								{
									header: "leave_meeting", 
									data: { 
											meetingID: meetingID,
											userID: index
									} 
										
								}
									
									
									
							  ];
							  
							  
							  
							 
			for(var key in users[index].meeting_user) {
				
				
				if(users[index].meeting_user[key].meetingID == meetingID && users[users[index].meeting_user[key].userID] != null) {
					
				
					
							  
					sendMSGforUser(msg, users[index].meeting_user[key].userID);
					
					
					for(var key1 in users[users[index].meeting_user[key].userID].meeting_user) {
						if(users[users[index].meeting_user[key].userID].meeting_user[key1].userID == index && users[users[index].meeting_user[key].userID].meeting_user[key1].meetingID == meetingID) {
							users[users[index].meeting_user[key].userID].meeting_user[key1].accept = "0";
							break;
						}
					}
					
					users[users[index].meeting_user[key].userID].notifications.push(msg[0]);
						
						
					
				}
					
					//notifications
			
				if(users[index].meeting_user[key].meetingID == meetingID) {
					var query = "insert into notifications set userID='"+users[index].meeting_user[key].userID+"', msg='"+msg[0].data.msg+"'";
					handle_database(connection,query);
				}
					
					
		
			} 
		}
		
	}

	
	
	
	/*
	
	
	
	
	
	
	
	
	**/
	
		
});
